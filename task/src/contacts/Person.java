package contacts;

public class Person extends BaseEntity {

    private String birth;
    private String gender;
    private String name;
    private String surname;

    public String getBirth() {
        return birth;
    }

    public void setBirth(String birth) {
        this.birth = birth;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    private Person(String dateOfBirth, String gender, String surname, String name, String phone) {

        super(phone);
        this.birth = dateOfBirth;
        this.gender = gender;
        this.surname = surname;
        this.name = name;
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("Name: ").append(displayValue(name)).append(System.lineSeparator())
                .append("Surname: ").append(displayValue(surname)).append(System.lineSeparator())
                .append("Birth date: ").append(displayValue(birth)).append(System.lineSeparator())
                .append("Gender: ").append(displayValue(gender)).append(System.lineSeparator())
                .append(super.toString());
        return builder.toString();
    }

    @Override
    public String searchFieldValue() {
        return name + surname + birth + gender + super.getNumber();
    }

    @Override
    public String displayValue() {
        return name + " " + surname;
    }

    @Override
    public String editableField() {
        return "name, surname, birth, gender, number";
    }

    public static class PersonBuilder {

        private String dateOfBirth;
        private String gender;
        private String surname;
        private String name;

        private String phone;

        public PersonBuilder dateOfBirth(String dateOfBirth) {
            this.dateOfBirth = dateOfBirth;
            return this;
        }

        public PersonBuilder gender(String gender) {
            this.gender = gender;
            return this;
        }

        public PersonBuilder surname(String surname) {
            this.surname = surname;
            return this;
        }

        public PersonBuilder name(String name) {
            this.name = name;
            return this;
        }

        public PersonBuilder phone(String phone) {
            this.phone = phone;
            return this;
        }

        public Person build() {
            return new Person(dateOfBirth, gender, surname, name, phone);
        }
    }
}
