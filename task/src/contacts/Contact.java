package contacts;

public class Contact {

    private String name;
    private String surname;
    private String phone;

    public Contact(String name, String surname, String phone) {
        this.name = name;
        this.surname = surname;
        this.phone = phone;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }


    @Override
    public String toString() {
        if (phone.isEmpty()) {
            return String.format("%s %s, [no number]", name, surname);
        } else {
            return String.format("%s %s, %s", name, surname, phone);
        }
    }

    static class ContactBuilder {
        private String name;
        private String surname;
        private String phone;

        public ContactBuilder phone(String phone) {
            this.phone = phone;
            return this;
        }
        public ContactBuilder surname(String surname) {
            this.surname = surname;
            return this;
        }
        public ContactBuilder name(String name) {
            this.name = name;
            return this;
        }

        public Contact build() {
            return new Contact(name, surname, phone);
        }
    }
}
