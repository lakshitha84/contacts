package contacts;

import java.io.File;
import java.util.List;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {

        Contacts contacts = null;
        String filename = null;
        if (args.length == 0) {
            contacts = new Contacts();
        } else {
            filename = args[0];
            contacts = loadContacts(args[0]);
        }
        Scanner scanner = new Scanner(System.in);
        while (true) {
            System.out.println("[menu] Enter action (add, list, search, count, exit):");
            String action = scanner.nextLine();
                switch (action) {
                    case "add":
                        addContact(scanner, contacts);
                        if (filename != null) {
                            SerializationUtil.serialize(contacts, filename);
                        }
                        break;
                    case "remove":
                        remove(scanner, contacts);
                        break;
                    case "edit":
                        edit(scanner, contacts);
                        if (filename != null) {
                            SerializationUtil.serialize(contacts, filename);
                        }
                        break;
                    case "count":
                        count(contacts);
                        break;
                    case "list":
                        info(contacts, scanner);
                        break;
                    case "search":
                        performSearch(scanner, contacts);
                        break;
                    case "exit":
                        return;
                    default:

                }
            System.out.println();
        }

    }

    private static Contact addContact(Scanner scanner, Contacts contacts){

        System.out.println("Enter the type (person, organization):");
        String type = scanner.nextLine();
        switch (type) {
            case "person":
                Person person = getPersonInfoToAdd(scanner);
                contacts.add(person);
                break;
            case "organization":
                Organization organization = getOrganizationInfoToAdd(scanner);
                contacts.add(organization);
                break;
        }
        System.out.println("The record added.");
        System.out.println();
        return null;
    }

    private static Person getPersonInfoToAdd(Scanner scanner) {

        System.out.println("Enter the name:");
        String name = scanner.nextLine();
        System.out.println("Enter the surname:");
        String surname = scanner.nextLine();
        System.out.println("Enter the birth date:");
        String dateOfBirth = scanner.nextLine();
        if (dateOfBirth == null || dateOfBirth.isEmpty()) {
            System.out.println("Bad birth date!");
            dateOfBirth = "";
        }
        System.out.println("Enter the gender (M, F):");
        String gender = scanner.nextLine();
        if (gender == null || gender.isEmpty()) {
            System.out.println("Bad gender!");
            gender = "";
        }
        System.out.println("Enter the number:");
        String phone = scanner.nextLine();
        if (!PhoneNumberValidator.isValidPhone(phone)) {
            System.out.println("Bad number!");
            phone = "";
        }
        return new Person.PersonBuilder()
                .name(name)
                .surname(surname)
                .dateOfBirth(dateOfBirth)
                .gender(gender)
                .phone(phone).build();
    }

    private static Organization getOrganizationInfoToAdd(Scanner scanner) {

        System.out.println("Enter the name:");
        String name = scanner.nextLine();
        System.out.println("Enter the address:");
        String address = scanner.nextLine();
        System.out.println("Enter the number:");
        String phone = scanner.nextLine();
        return new Organization.OrganizationBuilder()
                .name(name)
                .address(address)
                .phone(phone).build();
    }

    private static void info(Contacts contacts, Scanner scanner) {
       List<BaseEntity> baseEntities = contacts.getContacts();
        listContacts(baseEntities);
        System.out.print("Enter index to show info: ");
        String index = scanner.nextLine();
        BaseEntity baseEntity = contacts.get(Integer.valueOf(index));
        System.out.println(baseEntity.toString());
        System.out.println("[menu] Enter action (edit):");
        String action = scanner.nextLine();
        if (action.equalsIgnoreCase("edit")) {
            edit(scanner, contacts, baseEntity);
        }

    }

    private static void listContacts(List<BaseEntity> baseEntities) {
        for (int i = 0; i < baseEntities.size(); i++) {
            BaseEntity baseEntity = baseEntities.get(i);
            if (baseEntity instanceof Person) {
                Person per = (Person)baseEntity;
                System.out.printf("%d. %s %s", i + 1,per.getName(), per.getSurname());
            } else if (baseEntity instanceof Organization) {
                Organization org = (Organization)baseEntity;
                System.out.printf("%d. %s", i + 1, org.getName());
            }
            System.out.println();
        }
    }

    private static void count(Contacts contacts){
        System.out.println(String.format("The Phone Book has %d records.", contacts.noOfContacts()));
    }

    private static void edit(Scanner scanner, Contacts contacts){

        if (contacts.noOfContacts() == 0) {
            System.out.println("No records to edit!");
        } else {
            listContacts(contacts.getContacts());
            System.out.println("Select a record:");
            String index = scanner.nextLine();
            if (!index.matches("\\d*")) {
                return;
            }
            BaseEntity baseEntity = contacts.get(Integer.valueOf(index));
            String field;
            if (baseEntity instanceof Person) {
                System.out.println("Select a field (name, surname, birth, gender, number):");
                field = scanner.nextLine();
                System.out.println("Enter " + field);
            } else {
                System.out.println("Select a field (address, number):");
                field = scanner.nextLine();
                System.out.println("Enter " + field);
            }
            String value = scanner.nextLine();
            if (field.equals("number") && !PhoneNumberValidator.isValidPhone(value)) {
                contacts.edit(Integer.valueOf(index), field, "");
                System.out.println("Wrong number format!");
            } else {
                contacts.edit(Integer.valueOf(index), field, value);
            }
            System.out.println("The record updated!");
            System.out.println();
        }
    }

    private static void remove(Scanner scanner, Contacts contacts){

        if (contacts.noOfContacts() == 0) {
            System.out.println("No records to remove!");
        } else {
            contacts.list();
            System.out.println("Select a record:");
            String index = scanner.nextLine();
            contacts.remove(Integer.valueOf(index));
            System.out.println("The record removed!");
        }
    }

    private static void performSearch(Scanner scanner, Contacts contacts) {

        System.out.println("[search] Enter action ([number], back, again):");
        while (true) {
            System.out.println("Enter search query:");
            String search = scanner.nextLine();
            List<BaseEntity> searchResult = contacts.search(search);
            System.out.println(String.format("Found %d results:", searchResult.size()));
            for (int i = 0; i < searchResult.size(); i++) {
                System.out.println(String.format("%d. %s", i + 1, searchResult.get(i).displayValue()));
            }
            System.out.println("[search] Enter action ([number], back, again):");
            String in = scanner.nextLine();
            if (in.matches("\\d*")) {
                BaseEntity baseEntity = contacts.get(Integer.valueOf(in));
                System.out.println(baseEntity.toString());
                while (true) {
                    System.out.println("[record] Enter action (edit, delete, menu):");
                    in = scanner.nextLine();
                    if (in.equalsIgnoreCase("menu")) {
                        break;
                    } else if (in.equalsIgnoreCase("edit")) {
                        edit(scanner, contacts, baseEntity);
                    }
                }
            } else if (in.equalsIgnoreCase("back")) {
                continue;
            }
            if (in.equalsIgnoreCase("menu")) {
                break;
            }
        }
    }

    private static void edit(Scanner scanner, Contacts contacts, BaseEntity baseEntity) {
        System.out.printf("Select a field (%s):", baseEntity.editableField());
        System.out.println();
        String field = scanner.nextLine();
        System.out.printf("Enter %s:", field);
        System.out.println();
        String value = scanner.nextLine();
        if (field.equals("number") && !PhoneNumberValidator.isValidPhone(value)) {
            contacts.edit(baseEntity, field, "");
            System.out.println("Wrong number format!");
        } else {
            contacts.edit(baseEntity, field, value);
        }
        System.out.println("Saved");
        System.out.println(baseEntity);
    }

    private static Contacts loadContacts(String filename) {
        File file = new File(filename);
        if (file.exists()) {
            Contacts contacts = SerializationUtil.deserialize(Contacts.class, filename);
            return contacts;
        } else {
            return new Contacts();
        }
    }
}

