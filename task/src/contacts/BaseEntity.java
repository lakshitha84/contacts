package contacts;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.List;

public abstract class BaseEntity implements Serializable {

    private static final long serializeUID = 1l;

    protected String number;
    private LocalDateTime createdAt;
    private LocalDateTime lastEditedAt;

    public BaseEntity(String phone) {
        this.number = phone;
        createdAt = LocalDateTime.now().withNano(0).withSecond(0);
        lastEditedAt = LocalDateTime.now().withNano(0).withSecond(0);
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public LocalDateTime getLastEditedAt() {
        return lastEditedAt;
    }

    public void setLastEditedAt(LocalDateTime lastEditedAt) {
        this.lastEditedAt = lastEditedAt;
    }

    public LocalDateTime getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(LocalDateTime createdAt) {
        this.createdAt = createdAt;
    }

    @Override
    public String toString() {

        StringBuilder builder = new StringBuilder();
        return builder.append("Number: ").append(displayValue(number)).append(System.lineSeparator())
                .append("Time created: ").append(createdAt.toString()).append(System.lineSeparator())
                .append("Time last edit: ").append(lastEditedAt.toString()).append(System.lineSeparator())
                .toString();
    }

    protected String displayValue(String value) {
        return value == null || value.isEmpty() ? "[no data]" : value;
    }

    public abstract String searchFieldValue();

    public abstract String displayValue();

    public abstract String editableField();
}
