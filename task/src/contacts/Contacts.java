package contacts;

import java.io.Serializable;
import java.lang.reflect.Field;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

public class Contacts implements Serializable {

    private List<BaseEntity> contacts;

    public Contacts() {
        this.contacts = new ArrayList<>();
    }

    public Integer noOfContacts(){
       return contacts.size();
    }

    public List<BaseEntity> getContacts() {

        return new ArrayList<>(contacts);
    }

    public BaseEntity get(Integer index) {
        return contacts.get(index - 1);
    }
    public void edit(Integer index, String field, String value){
        BaseEntity contact = contacts.get(index - 1);
        try {
            if (field.equalsIgnoreCase("number")) {
                contact.setNumber(value);
            } else {
                Field clazzFiled = contact.getClass().getDeclaredField(field);
                clazzFiled.setAccessible(true);
                clazzFiled.set(contact, value);
            }
            contact.setLastEditedAt(LocalDateTime.now().withNano(0).withSecond(0));
        } catch (NoSuchFieldException e) {
            throw new RuntimeException(e);
        } catch (IllegalAccessException e) {
            throw new RuntimeException(e);
        }
        contact.setLastEditedAt(LocalDateTime.now());
    }

    public void edit(BaseEntity entity, String field, String value){
        try {
            if (field.equalsIgnoreCase("number")) {
                entity.setNumber(value);
            } else {
                Field clazzFiled = entity.getClass().getDeclaredField(field);
                clazzFiled.setAccessible(true);
                clazzFiled.set(entity, value);
            }
            entity.setLastEditedAt(LocalDateTime.now().withNano(0).withSecond(0));
        } catch (NoSuchFieldException e) {
            throw new RuntimeException(e);
        } catch (IllegalAccessException e) {
            throw new RuntimeException(e);
        }
        entity.setLastEditedAt(LocalDateTime.now());
    }

    public void remove(Integer index){
        contacts.remove(index - 1);
    }

    public void add(BaseEntity contact){
        contacts.add(contact);
    }

    public void list() {
        for (int i = 0; i < contacts.size(); i++) {
            System.out.printf("%d. %s", i+1, contacts.get(i).toString());
            System.out.println();
        }
    }

    public List<BaseEntity> search(String search) {

        Pattern searchPattern = Pattern.compile(search, Pattern.CASE_INSENSITIVE);
        return contacts.stream().filter(baseEntity -> {

            Matcher matcher = searchPattern.matcher(baseEntity.searchFieldValue());
            return matcher.find();
        }).collect(Collectors.toList());
    }
}
