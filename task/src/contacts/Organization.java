package contacts;

public class Organization extends BaseEntity {

    private String name;
    private String address;

    public Organization(String phone, String name, String address) {
        super(phone);
        this.name = name;
        this.address = address;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        return builder.append("Organization name: ").append(displayValue(name)).append(System.lineSeparator())
                .append("Address: ").append(displayValue(address)).append(System.lineSeparator())
                .append(super.toString())
                .toString();
    }

    @Override
    public String searchFieldValue() {
        return super.getNumber() + name + address;
    }

    @Override
    public String displayValue() {
        return name;
    }

    @Override
    public String editableField() {
        return "name, address, number";
    }

    public static class OrganizationBuilder {
        private String name;
        private String address;
        private String phone;

        public OrganizationBuilder name(String name) {
            this.name = name;
            return this;
        }

        public OrganizationBuilder address(String address) {
            this.address = address;
            return this;
        }

        public OrganizationBuilder phone(String phone) {
            this.phone = phone;
            return this;
        }

        public Organization build() {
            return new Organization(phone, name, address);
        }

    }
}
