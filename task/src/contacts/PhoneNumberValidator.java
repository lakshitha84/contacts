package contacts;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class PhoneNumberValidator {

    public static boolean isValidPhone(String phone) {

        String[] groups = phone.split("[\\s-]");
        groups[0] = groups[0].replaceAll("^\\+", "");
        //firt group has +
//
//        if (!startWithPlusOrNotMatcher.find()) {
//            return false;
//        }
        Pattern wrappedWithParenthesesPattern= Pattern.compile("^\\(\\w+\\)$");
        int count = 0;
        for (int i = 0; i < groups.length; i++) {
            Matcher wrappedWithParenthesesMatcher = wrappedWithParenthesesPattern.matcher(groups[i]);
            if ( i < 2 && wrappedWithParenthesesMatcher.matches()) {
                count++;
            }
            if (i >= 2 && wrappedWithParenthesesMatcher.matches()) {
                return false;
            }
        }
        if (count > 1) {
            return false;
        }
        Pattern atLeastOneCharPattern = Pattern.compile("^\\(\\w+\\)$|\\w+");
        Matcher atLeastOneCharMatcher = atLeastOneCharPattern.matcher(groups[0]);
        if (!atLeastOneCharMatcher.matches()) {
            return false;
        }
        Pattern atLeastTwoCharPattern = Pattern.compile("^\\(\\w{2,}\\)$|\\w\\w+");
        for (int i = 1; i < groups.length ; i++) {
            Matcher atLeastTwoCharMatcher = atLeastTwoCharPattern.matcher(groups[i]);
            if (!atLeastTwoCharMatcher.matches()) {
                return false;
            }
        }
//        Pattern parenthesesInsidePattern= Pattern.compile(".+\\(.+\\).+");
//        for (String phonePart: phoneNumberParts) {
//            Matcher parenthesesInsideMatcher = parenthesesInsidePattern.matcher(phonePart);
//            if (parenthesesInsideMatcher.matches()) {
//                return false;
//            }
//        }
        return true;
    }
}
